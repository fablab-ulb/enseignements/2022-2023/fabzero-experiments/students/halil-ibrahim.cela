# 3. Impression 3D

Ce cours a pour objectif de nous familiariser avec l'impression 3D et d'apprendre les avantages et aussi les limitations de cette technique. Au FabLab, nous avons eu l'occasion d'utiliser les imprimantes Prusa.

## 3.1 Avant la séance

il nous a été demandé de lire attentivement le tuto impression 3D écrit par Axel Cornu, on y apprend notamment les caractéristiques de nos imprimantes tel que les dimmensions du plateau et la taille de la buse mais également à comment bien préparé un G-code étape par étape. Le G-code est le code envoyé à l'imprimante avec tout les paramètres d'impression, celui-ci est généré à partir d'un fichier STL que l'on importe directement sur le logiciel PrusaSlicer qui est opensource et compatible avec la Prusa. 

### 3.2 Durant la séance

Axel a commencé par nous faire un rappel de tout ce qui était expliqué dans son tuto et il a insisté sur les points importants comme les supports, le remplissage, les bordures, les angles et nous a montré tout cela au moyen de pièce exemple qu'il avait imprimé au préable. Ensuite c'était à nous de jouer c'est à dire qu'on a pu utiliser les imprimantes afin de produire la pièce designer au module précédent. La première étape consiste à préparer son G-code à partir du fichier STL de notre pièce. Voici un exemple d'étape à suivre pour préparer son G-code :

- importer le fichier STL sur le logiciel PrusaSlicer
- placer sa pièce sur la zone d'impression de sorte à n'avoir que des zones vertes
- sélectionner la bonne imprimante
- sélectionner le bon filament
- sélectionner la bonne hauteur de couche qui est généralement 0.2mm ce qui donne un bon compromis entre vitesse et qualité d'impression
- vérifier que la pièce est bien imprimable sinon ajouter des supports
- appuyer sur découper maintenant 
- vérifier les paramètres
- appuyer sur exporter le G-code

![](images/module03/Prusa1.JPG)
![](images/module03/Prusa2.JPG)

on peut y voir le temps d'impression à chaque étape.




