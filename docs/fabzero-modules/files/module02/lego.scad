/*
* "FlexCatapult"
* Copyright (c) [2023], Halil-Ibrahim Cela
*
* Ce projet est sous licence Creative Commons Attribution-ShareAlike [Attribution-ShareAlike 4.0 International]
* Pour voir une copie de cette licence, visitez https://creativecommons.org/licenses/by-sa/4.0/legalcode
*/




width = 8;
width_beam=1;
height_beam=1;
height = 2;
length = 20;
rayon_cylinder=3;
rayon_lego=2.4;
espace_lego=3.2;
length_beam=40;

difference(){
    $fn=50;
minkowski()
{
    
cube([length,width,height],center=true);
cylinder(r=rayon_cylinder,h=height);
}

union(){
    
   rotate([0,0,90])
translate([0,4,0])
cylinder(h=10,r=rayon_lego,center=true);
    
    rotate([0,0,90])
translate([0,-4,0])
cylinder(h=10,r=rayon_lego,center=true);
   
    
}

    
    }
    
    translate([length+length_beam+rayon_cylinder,0,0])
difference(){
    $fn=50;
minkowski()
{
    
cube([length,width,height],center=true);
cylinder(r=rayon_cylinder,h=height);
}

union(){
    
   rotate([0,0,90])
translate([0,4,0])
cylinder(h=10,r=rayon_lego,center=true);
    
    rotate([0,0,90])
translate([0,-4,0])
cylinder(h=10,r=rayon_lego,center=true);
   
    
}

    
    }
    translate([length/2+rayon_cylinder,-width_beam/2,0])
    cube([length_beam,width_beam,height]);






    
