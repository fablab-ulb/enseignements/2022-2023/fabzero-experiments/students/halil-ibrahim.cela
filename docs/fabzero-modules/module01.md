# 1. Gestion de projet et documentation

Le premier module du cours FabZero a pour objectif de nous familiariser à des outils de gestion de projet et de documentation très utile à la bonne réalisation de tout projet. tout au long de ce cours nous allons également apprendre divers compétence utile pour notre projet final, c'est pourquoi une bonne documentation nous permettra de nous remémorer ce que nous avons appris mais également d'apprendre de nos camarades.

de part mon curcus, j'avais déjà utilisé git pour un cours d'informatique donc l'installation était déjà faite sur mon pc par contre je n'avais jamais utilisé le language markdown pour la réalisation d'un site web. 

## qu'est ce que git,github et gitlab ? 

Pour mieux comprendre ces 3 mots, il faut d'abord savoir ce que c'est le control de version. Le controle de version permet à plusieurs personnes travaillant sur un même projet principal d'apporter des modifications à des parties de projet séparément sur leur machine respective et ensuite de les fusionner en un seule et même projet. Maintenant que l'on sait cela, **Git** est un logiciel de gestion de version décentralisé c'est à dire qu'il est open source, libre et gratuit. **GitHub** est une entreprise a but lucratif qui offre un service d'hébergement de référentiel Git basé sur le cloud.L'interface GitHub est facile d'utilisation c'est pourquoi elle est énormément utilisé par les codeurs et même par les écrivains pour écrire des livres. GitLab est similaire à GitHub à part que Gitlab se présente plus comme un logiciel libre et GitHub plus comme un réseaux social. Dans le cadre de ce cours, nous allons utiliser Git et Gitlab.

## SSH Key

Pour ma part l'installation de Git et de Git Bash était déjà faite sur mon pc, la première étape pour moi consistait donc à cloner ma répository Gitlab sur mon pc. Pour Cela j'ai du apprendre ce que c'était qu'une SSH Key, je me suis rendu sur le site de [SSH](https://www.ssh.com/academy/ssh-keys).

fonctionnelement une SSH key est comme un mot de passe, cela permet de donner accès mais aussi de contrôler qui à accès à quoi. techniquement les key sont cryptographic utilisant le principe de key publique et privé. la key publique est celle qui autorise l'accès, on peut voir cela comme une serrure que la key privé peut ouvrir ou non.

Je n'avais jamais utilisé de SSH Key auparavant et donc j'ai dû en généré une en utilisant des commandes git en suivant ce [tuto](https://docs.gitlab.com/ee/user/ssh.html).ensuite il ne restait plus qu'à copié le contenu du fichier .pub dans la section SSH Key de notre compte GitLab et Git reconnaissait maintenant mon pc et mon compte Gitlab comme on peut le voir sur l'image ci dessous

![identi_git](images/module01/identi_git.PNG)

## problème de clonage

maintenant que j'étais lié à mon compte gitlab et que je pouvais travailler en local, j'ai commencé par vouloir cloner ma repository directement sur mon pc via la clé SSH et cela m'a donné cette erreur ci dessous

![](images/module01/prob_clone.PNG)

j'ai passé du temps à essayer de débugger cela mais rien n'y faisait, les solutions que je trouvais sur google concernait la clé SSH mais pour moi ce n'était pas le problème car mon compte gitlab était bien reconnu. Alors j'ai décidé de cloner via HTTPS et là ça a fonctionné du premier coup et à mon grand étonnement mon mot de passe gitlab ne m'a pas été demandé lors du clonage ( le mot de passe gitlab est demandé pour la clé HTTPS) si quelqu'un a une réponse sur ce bug qu'il n'hésite pas à m'en faire part. 

## commande Git 

une fois ma repository cloner ( non sans mal ), j'ai commencé à bosser en local et pour interagir avec Git et mettre à jour ce que je faisais sur Gitlab rien de plus simple il suffit d'ouvrir Git CMD et d'utiliser les quelques commandes utiles ci-dessous.


| Commande |  Description   | 
|-----|-----------------|---------|--------------------------|--------|
| git Add -A  | Ajouter tout les changements effectué sur les fichiers  |  22.00 $| http://amazon.com/test   |    Order many    |
| git commit -m "message d'envoie"  | permet de valider les chamgements apportés  |  22.00 $| http://amazon.com/test   |        |
| git add "nom fichier" | ajouter un fichier dans la repository  |  22.00 $| http://amazon.com/test   |        |
| git init | initialiser un depot  |  22.00 $| http://amazon.com/test   |        |
| git push   | envoyer les chamgements apportés à la branche principale associée |  22.00 $| http://amazon.com/test   |        |
| git pull   | fusionner le depot distant et la repo local |  22.00 $| http://amazon.com/test   |        |
| git status   | affiche la liste des fichiers modifiés |  22.00 $| http://amazon.com/test   |        |

## Markdown

markdown est un langage qui permet de faciliter l'écriture des pages web, en utilsant markdown nous pouvons avoir un control sur l'affichage du contenu par exemple il est très simple de mettre en gras, en italique, modifier la taille des titres, ajouter des images,... . markdown est rapidement devenu le standard pour un bon nombre de scientifiques et d'écrivains et en plus c'est simple et rapide à apprendre. Pour me lancer j'ai suivi ce [tuto](https://www.markdowntutorial.com/) cela m'a pris seulement 10 minutes et j'ai pu ensuite mettre en forme mes premières pages.


## les images

pour une bonne documentation, il est important d'illustrer notre propos avec des images concrètes. De nos jours, la qualité des images prise avec notre téléphone augmente de plus en plus et donc l'emplacement de stockage nécessaire deviens de plus en plus important. Pour diminuer l'espace utilisé, il nous a été demandé de réduire la taille de l'image en utilisant un logiciel pour cela j'utilise directement l'outil photo de windows qui permet de redimmensionner l'image mais aussi de la modifier. voici ci-dessous la fenêtre obtenu lors du redimensionnement sur l'outil windows.

![](images/module01/redimensionnement.JPG)

## Bilan 

une cheklist nous a été fourni pour vérifier que l'on a bien tout assimilé, la voici: 

1. Faire un site web et décrire comment il a été fait. ✔️
2. Présente toi. ✔️
3. Documente les étapes suivies pour télécharger les fichiers depuis le serveur Gitlab. ✔️
4. Documente les étapes suivies pour compresser les images pour qu'elles occupent un petit espace de stockage. ✔️
5. Documente comment tu compte utiliser les principes de management de projet. ✔️
6. Push ce que tu as fais sur le serveur. ✔️



