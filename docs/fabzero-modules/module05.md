# 5. Dynamique de groupe et projet final

## 5.1 Project Analysis and Design

Dans cette première partie de module 5, il nous a été demandé de réaliser l'arbre des problèmes et l'arbre des solutions concernant une thématique qui nous intéresse. Pour ce faire j'ai pioché dans les sujets qui nous ont été proposé et j'ai finalement choisi ceux des repair café. un Repair café est un lieu ou les citoyens se retrouve pour ne plus jeter leur objets mais pour essayer de les réparés ensemble. Ces Repair café sont soutenus par une ASBL appelée **Repair Together**. je vous met le lien de leur site web si vous voulez en apprendre plus [Repair Together](https://repairtogether.be/notre-association/).


### l'arbre des problèmes 

Pour apprendre ce que c'était j'ai regardé la vidéo disponible sur Youtube de 3 minutes et je me suis mis ensuite à chercher ma problématique principale qui représente le tronc de l'arbre ensuite j'ai identifié certaines causes représenté aux racines de l'arbre et enfin j'ai finis par chercher des conséquences représenté aux branches de l'arbre. Voici donc ce que j'ai obtenu en utilsant paint 

![](images/module05/arbre3.JPG)


### l'arbre des solutions

l'apprentissage s'est fait sur la même vidéo que pour l'arbre des probèmes, j'ai ensuite identifié un résultat souhaité qui est représenté au tronc de l'arbre puis des choses à mettre en place afin d'y arriver au niveau des racines et enfin l'impact qu'auront ces choses au niveau des branches. Voici donc ce que j'ai obtenu en utilsant paint 

![](images/module05/arbre4.JPG)

## 5.2 Group formation and brainstorming on project problems 

l'objectif de ce jour était de former les groupes et d'identifier des problématiques sur des thèmes choisis. Afin de faire cela, il a d'abord fallu ramener avec nous un object au choix, je n'en avais pas ramener mais j'ai réussi à trouver quelque chose qui m'intéressait dans la salle, c'était un pot de fleur fanné. celui-ci représentait pour moi une nature morte et une nature à qui il faut venir en aide par divers moyen possible. Ensuite nous avons eu l'occasion de voir les objets des autres et puis il nous a été demandé de nous diriger vers des objets qui nous interesse en rapport ou non avec notre propre objet. Je me suis dirigé vers quelqu'un que je ne connaissais pas avant et qui se prénomme Antoine, celui-ci avait ramener quelque chose de similaire à moi une branche sèche. Ensuite j'ai été vers quelqu'un que je connaissais déjà à savoir Eliot qui a ramenné avec lui une roche volcanique et qui m'a expliqué son choix ce qui m'a intéressé ensuite Martin que je ne connaissais pas non plus nous a rejoins et celui-ci avait une roche spécial. Nous étions donc 4 mais nous ne savions pas si cela allait notre groupe de projet, nous nous sommes installé au sol à coté d'un panneau blanc et des marqueurs de couleur. Nous avons commencé par y écrire des idées en rapports avec nos objets puis sélectionner certaines idées pour en faire des thèmes et puis pour chacun des thèmes nous avons lister des problématique. Ensuite en suivant une ligne décisionnelle nous avons choisi 2 thèmes principaux et avons ensuite barrer certaine problématique. A la fin de cette séance nous nous retrouvons donc avec un groupe et des problématique sur 2 thèmes concret à savoir l'escalade et l'eau.

Ci dessous on peut y voir tout d'abord nos différentes thématique avec leurs problématiques suivie de la ligne décisionnelle et enfin une photo de groupe 

![](images/module05/thematique.jpg)



![](images/module05/ligne.jpg)


![](images/module05/groupe.jpg)

## 5.3 Group dynamics

durant ce cours, nous avons appris divers outils concernant la communication, la gestion de projet et la prise de décision au sein d'un groupe. Une chose que j'ai apprécié c'est que l'on a eu l'occation de pratiquer en groupe ce que l'on a appris durant ce cours entre chaque point important. voici en bref ce que l'on a appris :

### prise de décision

* consensus
* température: utilisé des geste pour indiquer son niveau d'accord avec l'orateur, cela permet d'avoir une vue rapide d'ensemble de ce que chacun pense.
* vote majoritaire
* sans objection 

### outils de communication

* bâton de parole : seulement la personne possédant l'objet de parole peut parler
* temps de parole limité : chacun parle durant un temps imparti
* chacun son tour : chacun parle l'un après l'autre
* montrer que l'on veut dire quelque chose à l'aide de ses doigts qui donne la position dans la file 

### gestion de projet 

un exemple de réunion type nous a été donné : 

1) météo d'entrée : chacun parle de comment il se sent en arrivant cela permet par exemple de post poser des choses importantes qui devaient être décidé si une personne ne se sent pas au top.
2) Rôle : les rôles des participants sont définis ( secrétaire,gestionnaire du temps, l'animateur).
3) donner l'agenda du jours.
4) la réunion en elle même.
5) prévoir la prochaine réunion.
6) météo de sortie: pour voir si certaines craintes se sont établie ou au contraire si certaines sont parties

