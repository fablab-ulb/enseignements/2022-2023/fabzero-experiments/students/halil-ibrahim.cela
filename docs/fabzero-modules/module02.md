# 2. Conception Assistée par Ordinateur (CAO)

ce 2ème cours FabZero a pour objectif de nous familiariser avec des logiciels de CAO ainsi que avec les droits d'autheur Creative Commons. Pour ma part j'ai déjà utiliser durant mon cursus des logiciels de CAO tel que autocad et solidworks mais je n'avais encore jamais utilisé les logiciels de la formation qui sont opensource.

## 2.1 Logiciel de CAO

les CAO sont l'ensemble des logiciels et des techniques de modélisation géométrique qui permettent de concevoir et de tester virtuellement à l'aide d'un ordinateur toutes sortes de pièces. Dans cette formation, on nous a parlé de 3 logiciels CAO opensource en particulier à savoir :

- *[**InkScape**](https://inkscape.org/fr/) : inksape c'est du 2D vectoriel comparable à autocad mais pour des logos et des illustrations plus particulèrement.
- *[**OpenSCAD**](https://openscad.org/) : OpenSCAD c'est pour du 3D comme solidworks mais il faut programmer en utilisant le language openscad.
- *[**FreeCAD**](https://www.freecad.org/) : FreeCAD c'est comme solidworks mais en opensource.

## 2.2 mécanisme compliant

un mécanisme compliant en mécanique permet de transmettre une force et un mouvement en utilisant les propriétés élastiques de la matière. Ces mécanismes présentent de nombreux avantages par rapport à leur homologue classique comme par exemple :

- la réduction du nombre de pièces d'un assemblage
- procédé de production moins coûteux 
- précision du mouvement
- taille très réduite grâce au procédé de photolithographie
- réduction de l'usure
- ...

pour ce cours, l'objectif est de produire un mécanisme flexible qui s'assemble avec des legos. Pour cela, je me suis inspiré d'un design trouvé sur [**flexlinks**](https://www.compliantmechanisms.byu.edu/flexlinks).

Voici le design que je vais reproduire sur openscad

![](images/module02/flexlinks.JPG)

c'est la pièce en bleu sur laquelle je vais me concentrer et la seule contrainte est que le mécanisme doit s'emboiter avec un lego.

## Conception sur OpenSCAD

j'ai tout d'abord commencé par lire le tuto de Nicolas De Coster sur openscad ensuite j'ai commencé à prendre en main le logiciel en jouant avec les diverses fonctions que j'ai trouvé sur cette [**cheat_sheet**](https://openscad.org/cheatsheet/). J'ai donc appris à faire des cubes, des cylindres, les translations, les enlèvement de matière, les assemblages,... tout ceci m'a ammené à produire ce code que j'ai essayé de rendre le plus paramétrique possible tout en respectant la contrainte du lego. 

 ![](images/module02/code1.JPG)
 ![](images/module02/code2.JPG)

 comme on peut le voir je n'ai pas utilisé les modules qui sont comparable aux fonctions dans n'importe quel language de programmation classique pour rendre le code plus compréhensible mais j'ai simplement utilisé les différences et les unions.

 Voici le résultats obtenu

 ![](images/module02/legoflex.JPG) 


## 2.3 Creative Commons 

Les licences Creative Commons sont des licenses qui régissent les conditions dans lesquelles une oeuvre peut être distribué et réutilisé. Celles-ci ont été établies par l'organisation Creative Commons et publiées pour la première fois en 2002. 

 ![](images/module02/licenses.JPG) 

 j'ai donc ajouter une licenses au début de mon code afin que les personnes du cours puissent distibuer,modifier et utiliser cette pièce.

![](images/module02/licenses2.JPG) 



