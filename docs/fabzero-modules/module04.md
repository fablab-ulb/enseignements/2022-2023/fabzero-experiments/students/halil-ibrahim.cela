# 4. Laser cutting

dans le cadre de ce projet, nous avons du choisir une formation à suivre parmis plusieurs choix et j'ai donc choisi la formation sur la découpe assisté par ordinateur.

## 4.1 avant la séance

Avant le jours J de la formation, il a fallu lire la documentation sur les découpeuses laser du FabLab écrite par Axel Cornu. Ceci a été important car on y apprend notamment plusieurs précautions concernant l'utilisation des machines mais aussi du matériaux utilisé pour la découpe.

## 4.2 durant la séance

Nous avons commencé par répondre à un questionnaire en ligne pour être sur que tout le monde soit bien au courant des précautions, ensuite le formateur nous as expliqué le foncionnement et les spécificités des 3 machines du FabLab. Par exemple, pour la lasersaur il faut effectuer le calibrage du laser manuellement avant chaque découpe à l'aide des pièces déjà disponible dans le labo.

Ensuite nous avons été divisé en 3 groupes donc 1 par machine et nous avons du effectué ce qu'on appel une grille de test, cela consiste à faire varié pour un matériaux donné les paramètres de puissance et de vitesse pour voir les paramètres optimaux pour la gravure et pour la découpe de notre matériaux. En effet les deux réglages indispensable pour réaliser une découpe sont la vitesse et la puissance, une vitesse élevé impliquera un travail plus rapide mais potentiellement de ne pas couper la matière et une puissance élevé implique plus de chance de découper la matière mais une précision plus faible si la matière brûle.

Pour effectuer ce test nous avons trouvé une plaque de plexiglas dans le labo ainsi qu'un fichier test déjà fais sur google que nous avons récupérer au format svg. Le format svg permet d'éffectuer des découpes vectorielles c'est à dire que le laser va suivre le dessin vectorielle 2D réalisable par exemple sur inkscape. Voici ci dessous notre dessin test 

![](images/module04/testlaser.JPG)

on peut y voir que certaine parties sont en noit et d'autre sont en rouge, c'est un point fort de la découpe vectorielle qui permet de faire du color mapping c'est à dire que l'on peut attribuer différents paramètre en fonction de la couleur au sein d'une même découpe. Voici le résultat que nous avons obtenu.

![](images/module04/resultatlaser.JPG)


## 4.3 Kirigami

Dans le cadre de mon TFE, je travaille sur la récupération d'eau au moyen de fillet à nuage avec le FrugalLab. Ces fillets à nuage sont réalisé au FrugalLab en découpant du mylar 250 um d'épaisseur gràce à la découpe laser en suivant un certain pattern spécifique ce qui en fait un kirigami. après avoir dessiner le pattern sur un logiciel tel que inkscape on peut envoyer le fichier svg récupéré à la découpeuse laser en sélectionnant les bons paramètres corresponant au mylar. Voici ce que l'on obtient. 

 ![](images/module04/kirigami.JPG)
