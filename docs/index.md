
Bienvenue sur ma page FabZero !

cette page a pour objectif de documenter mon parcours tout au long de ce cours, durant la première séance obligatoire il nous a été expliqué que ce qui importe n'est pas vraiment le résultat mais plus le processus pour y arrivé c'est pourquoi la documentation de celui-ci est très important. Plusieurs formation seront organisé à fin de nous apprendres des outils nécessaire à la bonne réalisation de notre projet à savoir :

    1. Gestion de projet et documentation

    2. Conception Assistée par Ordinateur

    3. Impression 3D

    4. Outil Fab Lab sélectionné

    5. Dynamique de groupe et projet final



## quelques petites choses sur moi

Je m'appelle Halil-Ibrahim.j'ai 23 ans et je suis actuellement en 2ème année de master à l'EPB dans la finalité biomédicale. Je suis quelqu'un de calme, gentil, sérieux, organisé ( dans ma tête), ponctuel, ambitieux,... . Je suis quelqu'un qui s'interesse à tout dans le sens où j'aime apprendre toute sorte de choses utile dans la vie et j'aime la difficulté c'est à dire que je chercherai toujours à me challenger. 

## Mes objectifs dans ce cours

Je me suis inscris à ce cours car j'aimerais apprendre des outils utile pour la conception rapide ( important pour l'ingénieur) mais également pour le fait qu'il y a un projet à réaliser ce qui va me permettre de mettre tout les outils en pratique et de travailler en équipe sur une problématique commune où l'on va pouvoir laisser place à la créativité et au challenge !!!!
